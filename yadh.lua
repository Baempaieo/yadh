#!/usr/bin/lua

-- [[
-- This is a redone port from scratch of YADH, specifically built in mind for Android devices with termux installed.
-- Just to make stuff simpler, this is straight-up a library-less port: so instructions would be easier to follow
-- (including the readme)
-- ]]

local logs = " /data/data/com.termux/files/usr/var/log/apt/history.log"
local cache = " /data/data/com.termux/files/usr/var/cache/apt/archives"

io.write([[
            .-----------------------------.
  -''''-    |   y   y            d h      | 
 .  ..  \   |   yyyyy  aaaa   dddd h      |
 |  ' _ .   |       y a aa a d   d hhhh   |
  \      c  |     yy  a    a  dddd h  h   |
   .    /   | --------------------------- |
       c    |  Yet Another Debian Helper  |  
            '-----------------------------'

Options:
-n:  Displays the number of packages installed 
-p:  Purge residue packages on your system
-c:  Clean APT's cache
-l:  Show APT's history logs
-e:  Get extra info about YADH!
-q:  Quit the script

Pick an option: ]])
local option = io.read()

local function pkgs()
		print("Number of packages installed: ")
		os.execute("dpkg-query -f '${binary:Package} \n' -W | wc -l")
end

local function purge()
		os.execute("apt purge $(dpkg -l | grep '^rc' | awk '{print $2}')")
end

local function cache()
		print("Before cleaning the cache, here's the size of the cache: \n")
		os.execute("du -mh" .. cache)
		io.write("Do you want to clean the cache? [Y/n] ")

		local cache_choice = io.read()
				if (cache_choice == "y" and "Y") then
						os.execute("apt clean")
						print("Cleaned!")
				elseif (cache_choice == "n" and "N") then
						print("Alright then")
				else
						print("Whoops, wrong option: try again!")
						return cache()
				end
		end 

local function history()
		os.execute("less" .. logs)
end

if (option == "n") then
		pkgs()
elseif (option == "p") then
		purge()
elseif (option == "c") then
		cache()
elseif (option == "l") then
		history()
elseif (option == "e") then
		 print([[

- Version of YADH: 2.0-1 [Reworked build]
- Author:          Baempaieo
- Repository:      https://codeberg.org/Baempaieo/yadh
- Build date:      29th August 2024
- Ported language: Lua
		]])
elseif (option == "q") then
		print("Exiting YADH...")
else 
		print("Wrong option, quitting YADH...")
end
