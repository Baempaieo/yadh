# YADH - Yet Another Debian Helper
**YADH** is a tiny script that eases up simple actions on Debian (and Debian-based) systems.  
Formerly was created as my very first bash script, all for my own, but nowadays shared to the world for anyone to use on their system.  

## Features
With **YADH**, you can wrap up lots of long-stringed commands into simple arguments you can give to the tool.  
So far, the following could be done:  
* Check the amount of packages installed on your system;  
* Purge even more packages that your system normally couldn't without human intervention;  
    * This is typically referred if commands from APT, like `autoclean`, `autopurge`, and `autoremove` won't do any justice if your system flies off-the-radar useless system packages;  
* Check and clean up APT's cached data;  
* Give a peek to APT's package history;  
    * With this, you can see if APT may leave behind some unpurged packages onto your system, too- to add, this is also a good alternative (even if barebones) if you'd like switching to **Nala** because of the `history` feature;  
* (Just an extra) see info about the utility, including the license;  

## Installation
```
git clone https://codeberg.org/Baempaieo/yadh
cd yadh
chmod +x ./yadh
```

Otherwise, if you'd like having the script as a system binary that can be invoked anytime
```
chmod 755 ./yadh
sudo mv yadh /usr/bin
```

## Manpage
If you'd like obtaining a manpage of the utility for occasional offline consumption, in the repo you may find something under the `/doc` folder.  

If you'd like to import the manpage within your system, feel free to copy and paste these commands
```
cd doc/
sudo cp yadh.1 /usr/local/share/man/man1/
```

## Ports
In any case someone would like to run **YADH** under systems like Android and such, the utility has even a port to Lua remade from scratch- this can be freely findable under the name of `yadh.lua` into the repo.  

The point of the port has been, initially, an experiment on converting the script to another language:  
but nowadays moreso repurposed just to run on a limited environment like Android (under Termux).  

### Installation (optional)
```
## This is just an initial step to ensure the system packages are updated.
## You're free to skip it, at your own choice
apt update; apt upgrade; apt autoremove; apt autopurge

apt install lua54
git clone https://codeberg.org/Baempaieo/yadh
cd yadh
cp yadh.lua ~/
```

### What has happened to the `argparse` port?
Since i want to keep the repo a lil bit simpler (and lighter), i have decided to make the Lua port of the script straight-up without the usage of external libraries (either by manually importing it, or using the `luarocks` plugin manager).  

To add to the reason, i feel mantaining the same ported script into 2 different files won't be much of a good idea:  
since i aim more to have the script usable by having lesser hassles on setup and do not overflow the thing with extra dependencies, the `argparse` port got dropped by my own free will.
